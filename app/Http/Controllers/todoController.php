<?php

namespace App\Http\Controllers;
use \App\Todo;
use Illuminate\Http\Request;

class todoController extends Controller
{
    public function index()
    {
        $todos = Todo::all();
        return view('index' , compact('todos'));
    }

    public function store(Request $request)
    {
        $todo = new Todo;
        $todo->title = $request->title;
        $todo->desc = $request->desc;
        $todo->save();

        return redirect()->back();
    }
}
