<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css">
    <title>@yield('title')</title>
      @yield('header')
  </head>
  <body>

  <!-- ============================= Navbar start ============================= -->
      <nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top">
          <div class="container">
              <a class="navbar-brand" href="index.html">{{ config('app.name') }}</a>
              <button  class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header_nav" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
              </button>

              <div class="collapse navbar-collapse" id="header_nav">
                  <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a class="nav-link" href="#">demo</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">demo</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">demo</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">demo</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">demo</a></li>
                  </ul>
              </div>
          </div>
      </nav>
      <!-- ============================= Navbar End ============================= -->

    @yield('content')


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"></script>
    @yield('footer')
  </body>
</html>