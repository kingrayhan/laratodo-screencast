@extends('layouts.app')


@section('title' , 'Lara Todo')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6 ml-auto mr-auto mt-5">


                <div class="list-group">

                    @foreach($todos as $todo)

                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start {{ $todo->iscompleted ? 'active' : '' }}">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">{{ $todo->title }}</h5>
                            <small>{{ $todo->created_at->diffForHumans()  }}</small>
                        </div>
                        <p class="mb-1">{{ $todo->desc }}</p>
                    </a>

                    @endforeach



                    <!-- Button trigger modal -->
                        <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#exampleModal">
                            Add new Task
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form action="/" method="post">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                            {{ csrf_field() }}
                                            <!-- @inputfield title -->
                                             <div class="form-group">
                                                    <label for="title">Title</label>
                                                    <input type="text" class="form-control" id="title" name="title" placeholder="Title">
                                            </div>
                                            
                                            <!-- @textfield description -->
                                             <div class="form-group">
                                                    <label for="description">Description</label>
                                                    <textarea  id="description" cols="30" rows="10" name="desc" class="form-control" placeholder="Description"></textarea>
                                            </div>
                                            


                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- End of modal -->

                </div>
                <!-- End of list-group -->






            </div>
        </div>
    </div>

@stop